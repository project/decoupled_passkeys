# Decoupled Passkeys (Webauthn)

## Overview

Adds JSON:RPC endpoints for allowing users to register/login with Webauthn in a
decoupled configuration.

For the passkey display name, the Drupal user account display name is normally
used. If the Email Registration module is enabled, the email address is used
instead.

## Installation

Install the module via composer.

Configure permissions.

Registration permission is `create public key credential source entities` from
the Public Key Credential Source module.

Login permission is `login by passkey` for this module.

## JSON:RPC endpoints

### Registration

To get the registration options, call `user.register_device_options` with no
parameters.

Use the response to generate an attestation object. For example, if using
@simplewebauthn TS library, `startRegistration(response)`.

Then call `user.register_device` with the `attestation` object (stringified
JSON).

### Authentication

To get the request options, call `user.request_options` with the `user_handle`
parameter (uid as string).

Use the response to generate a request object. For example, if using
@simplewebauthn TS library, `startAuthentication(response)`.

Then call `user.authenticate_request` with the `request` object (stringified
JSON).

## UI

This module does not provide any kind of frontend.

For a good example and best practice recommendations, see here:

https://fidoalliance.org/ux-guidelines-for-passkey-creation-and-sign-ins/

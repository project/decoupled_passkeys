<?php

declare(strict_types=1);

namespace Drupal\decoupled_passkeys\Exception;

/**
 * Invalid argument exception for the Decoupled Passkeys module.
 */
class DecoupledPasskeysInvalidArgumentException extends DecoupledPasskeysException {
}

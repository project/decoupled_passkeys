<?php

declare(strict_types=1);

namespace Drupal\decoupled_passkeys\Exception;

/**
 * Exceptions for the Decoupled Passkeys module.
 */
class DecoupledPasskeysException extends \Exception {
}

<?php

declare(strict_types=1);

namespace Drupal\decoupled_passkeys\Exception;

/**
 * Unexpected value exception for the Decoupled Passkeys module.
 */
class DecoupledPasskeysUnexpectedValueException extends DecoupledPasskeysException {
}

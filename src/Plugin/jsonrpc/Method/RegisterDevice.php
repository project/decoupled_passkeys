<?php

declare(strict_types=1);

namespace Drupal\decoupled_passkeys\Plugin\jsonrpc\Method;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\decoupled_passkeys\Exception\DecoupledPasskeysUnexpectedValueException;
use Drupal\jsonrpc\Exception\JsonRpcException;
use Drupal\jsonrpc\MethodInterface;
use Drupal\jsonrpc\Object\Error;
use Drupal\jsonrpc\Object\ParameterBag;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Drupal\public_key_credential_source\PublicKeyCredentialSourceWebauthnInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Webmozart\Assert\Assert;

/**
 * Registers a passkey for a device.
 *
 * @JsonRpcMethod(
 *   id = "user.register_device",
 *   usage = @Translation("Registers a passkey for a device."),
 *   access = {"create public key credential source entities"},
 *    params = {
 *    "attestation" = @JsonRpcParameterDefinition(
 *      schema={"type"="string"},
 *      required=true
 *    ),
 *  }
 * ),
 */
final class RegisterDevice extends JsonRpcMethodBase {

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerFactory;

  /**
   * Constructs a new EntityResourceBase object.
   *
   * @param array $configuration
   *   JSON-RPC config.
   * @param string $plugin_id
   *   The plugin ID.
   * @param \Drupal\jsonrpc\MethodInterface $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The Watchdog logger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\public_key_credential_source\PublicKeyCredentialSourceWebauthnInterface $webauthn
   *   The Webauthn Framework class for public key credential source entities.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    MethodInterface $plugin_definition,
    protected AccountInterface $currentUser,
    LoggerChannelFactoryInterface $logger_factory,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected PublicKeyCredentialSourceWebauthnInterface $webauthn,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->loggerFactory = $logger_factory->get('decoupled_passkeys');
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): RegisterDevice {
    if (!$plugin_definition instanceof MethodInterface) {
      throw new DecoupledPasskeysUnexpectedValueException('Plugin definition should be MethodInterface!');
    }
    return new self(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('current_user'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('public_key_credential_source.webauthn')
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function execute(ParameterBag $params): true {
    try {
      $attestation = $params->get('attestation');
      Assert::stringNotEmpty($attestation);

      $uid = $this->currentUser->id();
      $user_storage = $this->entityTypeManager->getStorage('user');
      $account = $user_storage->load($uid);
      Assert::isInstanceOf($account, UserInterface::class);
      /** @var \Drupal\user\UserInterface $account */
      // Do not enable debugging on production!
      $this->loggerFactory->debug("Attempting to register a passkey for $uid");
      $this->webauthn->registerDevice($account, $attestation);

      return TRUE;
    }
    /* @phpstan-ignore-next-line Thrown exceptions in catch block must bundle previous exception. */
    catch (\LogicException $e) {
      $error_message = $e->getMessage();
      $this->loggerFactory->error($error_message);
      $error = Error::internalError();
      throw JsonRpcException::fromError($error);
    }
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function outputSchema(): array {
    return ['type' => 'boolean'];
  }

}

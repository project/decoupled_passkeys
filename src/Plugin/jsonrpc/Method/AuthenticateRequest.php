<?php

declare(strict_types=1);

namespace Drupal\decoupled_passkeys\Plugin\jsonrpc\Method;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\decoupled_passkeys\Exception\DecoupledPasskeysException;
use Drupal\decoupled_passkeys\Exception\DecoupledPasskeysUnexpectedValueException;
use Drupal\jsonrpc\Exception\JsonRpcException;
use Drupal\jsonrpc\MethodInterface;
use Drupal\jsonrpc\Object\Error;
use Drupal\jsonrpc\Object\ParameterBag;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Drupal\public_key_credential_source\PublicKeyCredentialSourceWebauthnInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Webauthn\PublicKeyCredentialSource as PKCredentialSource;
use Webmozart\Assert\Assert;

/**
 * Authenticates via passkey.
 *
 * @JsonRpcMethod(
 *   id = "user.authenticate_request",
 *   usage = @Translation("Authenticates a user via passkey."),
 *   access = {"login by passkey"},
 *   params = {
 *     "request" = @JsonRpcParameterDefinition(
 *       schema={"type"="string"},
 *       required=true
 *     ),
 *     "userHandle" = @JsonRpcParameterDefinition(
 *        schema={"type"="string"},
 *        required=true *
 *     ),
 *   }
 * ),
 */
final class AuthenticateRequest extends JsonRpcMethodBase {

  /**
   * Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $loggerFactory;

  /**
   * The request parameter.
   */
  private const string PARAMETER_REQUEST = 'request';

  /**
   * The webauthn userHandle parameter.
   */
  private const string PARAMETER_USER_HANDLE = 'userHandle';

  /**
   * Constructs a new EntityResourceBase object.
   *
   * @param array $configuration
   *   JSON-RPC config.
   * @param string $plugin_id
   *   The plugin ID.
   * @param \Drupal\jsonrpc\MethodInterface $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The Watchdog logger.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\public_key_credential_source\PublicKeyCredentialSourceWebauthnInterface $webauthn
   *   The Webauthn Framework class for public key credential source entities.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    MethodInterface $plugin_definition,
    protected AccountInterface $currentUser,
    LoggerChannelFactoryInterface $logger_factory,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected PublicKeyCredentialSourceWebauthnInterface $webauthn,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->loggerFactory = $logger_factory->get('decoupled_passkeys');
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): AuthenticateRequest {
    if (!$plugin_definition instanceof MethodInterface) {
      throw new DecoupledPasskeysUnexpectedValueException('Plugin definition should be MethodInterface!');
    }
    return new self(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('current_user'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('public_key_credential_source.webauthn')
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function execute(ParameterBag $params): false|string {
    try {

      // Ensure the parameters have been supplied.
      $authentication_request = $params->get(self::PARAMETER_REQUEST);
      Assert::stringNotEmpty($authentication_request, 'No value for request!');
      $user_handle = $params->get(self::PARAMETER_USER_HANDLE);
      Assert::stringNotEmpty($user_handle, 'No value for userHandle!');

      $pk_credential_source = $this->webauthn->authenticateRequest($user_handle, $authentication_request);

      if ($pk_credential_source instanceof PKCredentialSource) {
        $uuid = $pk_credential_source->userHandle;
        $user_storage = $this->entityTypeManager->getStorage('user');
        $account_array = $user_storage->loadByProperties(['uuid' => $uuid]);
        $account = reset($account_array);
        Assert::isInstanceOf($account, UserInterface::class);
        /** @var \Drupal\user\UserInterface $account */

        user_login_finalize($account);
        return json_encode($account);
      }
      throw new DecoupledPasskeysException('Failed to get PKCredentialSource!');
    }
    /* @phpstan-ignore-next-line Thrown exceptions in catch block must bundle previous exception. */
    catch (\LogicException $e) {
      $error_message = $e->getMessage();
      $this->loggerFactory->error($error_message);
      $error = Error::internalError();
      throw JsonRpcException::fromError($error);
    }
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function outputSchema(): array {
    return ['type' => 'string'];
  }

}
